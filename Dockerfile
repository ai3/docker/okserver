FROM golang:latest as build
ADD . /src
RUN cd /src && go build -tags netgo -o okserver okserver.go && strip okserver

FROM scratch
COPY --from=build /src/okserver /
CMD ["/okserver"]

