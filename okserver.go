package main

import (
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	// Get the address to bind on from the environment.
	addr := os.Getenv("ADDR")
	if addr == "" {
		addr = ":3100"
	}

	// Create a very simple HTTP server with two endpoints: one
	// that just replies "OK", and one that dumps the incoming
	// request back to the client.
	//
	// The OK handler answers with a cachable response, while the
	// dynamic one sets cache-busting headers.
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case "/echo":
			w.Header().Set("Content-Type", "text/plain")
			w.Header().Set("Expires", "-1")
			w.Header().Set("Cache-Control", "no-store")
			r.Write(w)
		case "/", "/health":
			w.Header().Set("Cache-Control", "max-age=86400")
			io.WriteString(w, "OK\n")
		default:
			http.NotFound(w, r)
		}
	})

	if err := http.ListenAndServe(addr, nil); err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
}
