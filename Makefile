all: build/okserver

build/okserver: okserver.go
	-mkdir -p build
	go build -o $@ -tags netgo -a -ldflags -w $^
